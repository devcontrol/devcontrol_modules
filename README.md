# devcontrol_modules

Módulos de Odoo para la gestión de libros - Una alternativa libre para la gestión de la comercialización de libros y gestión editorial

Creados por el colectivo Devcontrol


## Help us with a donation

https://amigues.llibres.prou.be/donaciones



## Info of the project

You can learn more about the project in this talk that we did in the 2023 hackmeeting. It's in Spanish.

https://www.arkiwi.org/path64/aGFja21lZXRpbmcvSGFja21lZXRpbmcgMjAyMyBEZXNlcnRTZXEgQmFyY2Vsb25hL2RwbC1saWJyb3Mtc2lubGkub2dn/html