We need to update this file. But right now when we need a release, we update the version number in the manifest of the modules that need to be published. 
No need to create tags.
The version truth is on the manifest file of each module.

Task: review the explanation below and see if it's still relevant.


# Releasing modules

Use this checklist to create a new release of the **`devcontrol_modules` project** and publish the pypi packages to the public pypi registry. All steps are intended to be run from the root directory of the repository.

## Creating a new release

1. Make sure you are currently on the `main` branch, otherwise run `git checkout main`.
2. `git pull` to make sure you haven’t missed any last-minute commits. After this point, nothing else is making it into this version.
3. Test the code in local.
4. Read the git history since the last release, for example via `git --no-pager log --oneline --no-decorate 13.0.0.2.0^..origin/main` (replace `13.0.0.2.0` with the last published version).
5. Condense the list of changes into something user-readable and write it into the `CHANGELOG.md` file with the release date and version, following the specification here on [how to write a changelog](https://keepachangelog.com/en/1.0.0/). Make sure you add references to the regarding PRs and issues. Note that the first two numbers correspond to the odoo version (i.e. `13.0`).
6. Update module version in the manifest.
7. Commit the `CHANGELOG.md` and manifest changes you've just made.
8. Create a git tag.
9. `git push origin main --tags` to push the tag to Framagit.
10. [Create](https://framagit.org/devcontrol/devcontrol_modules/-/releases/new) a new release on Framagit, select the tag you've just pushed under *"Tag version"* and use the same for the *"Release title"*. For *"Describe this release"* copy the same information you've entered in `CHANGELOG.md` for this release. See examples [here](https://framagit.org/devcontrol/devcontrol_modules/-/releases).

## Deploy a package on pypi

1. ...?
