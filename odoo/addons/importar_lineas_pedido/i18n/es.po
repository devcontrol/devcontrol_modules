# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* importar_lineas_pedido
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 13.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-16 05:32+0000\n"
"PO-Revision-Date: 2022-12-16 05:32+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: importar_lineas_pedido
#: code:addons/importar_lineas_pedido/importar_lineas_pedido.py:0
#: code:addons/importar_lineas_pedido/importar_lineas_pedido.py:0
#: code:addons/importar_lineas_pedido/importar_lineas_pedido.py:0
#, python-format
msgid "\"%s\" Tax not in your system"
msgstr "\"%s\" Impuestos no en su sistema"

#. module: importar_lineas_pedido
#: code:addons/importar_lineas_pedido/importar_lineas_pedido.py:0
#: code:addons/importar_lineas_pedido/importar_lineas_pedido.py:0
#, python-format
msgid ""
"%s product is not found\" .\n"
" If you want to create product then first select Import Product By Name option ."
msgstr ""

#. module: importar_lineas_pedido
#: code:addons/importar_lineas_pedido/importar_lineas_pedido.py:0
#, python-format
msgid "%s product is not found\"."
msgstr "%s no se encuentra el producto\"."

#. module: importar_lineas_pedido
#: model:ir.model.fields.selection,name:importar_lineas_pedido.selection__order_line_wizard__import_prod_option__barcode
msgid "Barcode"
msgstr "Código de barras"

#. module: importar_lineas_pedido
#: model:ir.model.fields.selection,name:importar_lineas_pedido.selection__order_line_wizard__import_option__csv
msgid "CSV File"
msgstr "Archivo CSV"

#. module: importar_lineas_pedido
#: model_terms:ir.ui.view,arch_db:importar_lineas_pedido.sale_order_wizard_view
msgid "Cancel"
msgstr "Cancelar"

#. module: importar_lineas_pedido
#: model:ir.model.fields.selection,name:importar_lineas_pedido.selection__order_line_wizard__import_prod_option__code
msgid "Code"
msgstr "Código"

#. module: importar_lineas_pedido
#: model:ir.model.fields,field_description:importar_lineas_pedido.field_order_line_wizard__create_uid
msgid "Created by"
msgstr "Creado por"

#. module: importar_lineas_pedido
#: model:ir.model.fields,field_description:importar_lineas_pedido.field_order_line_wizard__create_date
msgid "Created on"
msgstr "Creado en"

#. module: importar_lineas_pedido
#: model:ir.model.fields,field_description:importar_lineas_pedido.field_order_line_wizard__display_name
msgid "Display Name"
msgstr "Nombre para mostrar"

#. module: importar_lineas_pedido
#: model:ir.model.fields,field_description:importar_lineas_pedido.field_order_line_wizard__id
msgid "ID"
msgstr "IDENTIFICACIÓN"

#. module: importar_lineas_pedido
#: model_terms:ir.ui.view,arch_db:importar_lineas_pedido.sale_order_wizard_view
msgid "Import"
msgstr "Importar"

#. module: importar_lineas_pedido
#: model:ir.actions.act_window,name:importar_lineas_pedido.import_order_line_action
msgid "Import Order Lines"
msgstr "Importar líneas de pedido"

#. module: importar_lineas_pedido
#: model:ir.model.fields,field_description:importar_lineas_pedido.field_order_line_wizard__import_prod_option
msgid "Import Product By "
msgstr "Importar producto por"

#. module: importar_lineas_pedido
#: model_terms:ir.ui.view,arch_db:importar_lineas_pedido.sale_order_inherit_view
msgid "Import Sale Order Lines"
msgstr "Importar líneas de órdenes de venta"

#. module: importar_lineas_pedido
#: model:ir.model.fields,field_description:importar_lineas_pedido.field_order_line_wizard____last_update
msgid "Last Modified on"
msgstr "Última modificación el"

#. module: importar_lineas_pedido
#: model:ir.model.fields,field_description:importar_lineas_pedido.field_order_line_wizard__write_uid
msgid "Last Updated by"
msgstr "Actualizado por última vez por"

#. module: importar_lineas_pedido
#: model:ir.model.fields,field_description:importar_lineas_pedido.field_order_line_wizard__write_date
msgid "Last Updated on"
msgstr "Ultima actualización en"

#. module: importar_lineas_pedido
#: model:ir.model.fields.selection,name:importar_lineas_pedido.selection__order_line_wizard__import_prod_option__name
msgid "Name"
msgstr "Nombre"

#. module: importar_lineas_pedido
#: code:addons/importar_lineas_pedido/importar_lineas_pedido.py:0
#: code:addons/importar_lineas_pedido/importar_lineas_pedido.py:0
#, python-format
msgid "Please select any file or You have selected invalid file"
msgstr "Seleccione cualquier archivo o ha seleccionado un archivo no válido"

#. module: importar_lineas_pedido
#: model:ir.model.fields,field_description:importar_lineas_pedido.field_order_line_wizard__product_details_option
msgid "Product Details Option"
msgstr "Opción de detalles del producto"

#. module: importar_lineas_pedido
#: model:ir.model.fields,field_description:importar_lineas_pedido.field_order_line_wizard__import_option
msgid "Select"
msgstr "Seleccione"

#. module: importar_lineas_pedido
#: model:ir.model.fields,field_description:importar_lineas_pedido.field_order_line_wizard__sale_order_file
msgid "Select File"
msgstr "Seleccione Archivo"

#. module: importar_lineas_pedido
#: model:ir.model.fields.selection,name:importar_lineas_pedido.selection__order_line_wizard__product_details_option__from_product
msgid "Take Details From The Product"
msgstr "Obtener detalles del producto"

#. module: importar_lineas_pedido
#: model:ir.model.fields.selection,name:importar_lineas_pedido.selection__order_line_wizard__product_details_option__from_xls
msgid "Take Details From The XLS File"
msgstr "Obtener detalles del archivo XLS"

#. module: importar_lineas_pedido
#: model:ir.model.fields.selection,name:importar_lineas_pedido.selection__order_line_wizard__product_details_option__from_pricelist
msgid "Take Details With Adapted Pricelist"
msgstr "Obtenga detalles con la lista de precios adaptada"

#. module: importar_lineas_pedido
#: code:addons/importar_lineas_pedido/importar_lineas_pedido.py:0
#, python-format
msgid "UOM \"%s\" is Not Available"
msgstr "UOM \"%s\" no está disponible"

#. module: importar_lineas_pedido
#: code:addons/importar_lineas_pedido/importar_lineas_pedido.py:0
#: code:addons/importar_lineas_pedido/importar_lineas_pedido.py:0
#: code:addons/importar_lineas_pedido/importar_lineas_pedido.py:0
#, python-format
msgid "We cannot import data in validated or confirmed order."
msgstr "No podemos importar datos en un orden validado o confirmado."

#. module: importar_lineas_pedido
#: model:ir.model.fields.selection,name:importar_lineas_pedido.selection__order_line_wizard__import_option__xls
msgid "XLS File"
msgstr "Archivo XLS"

#. module: importar_lineas_pedido
#: model_terms:ir.ui.view,arch_db:importar_lineas_pedido.sale_order_wizard_view
msgid "or"
msgstr "o"

#. module: importar_lineas_pedido
#: model:ir.model,name:importar_lineas_pedido.model_order_line_wizard
msgid "order.line.wizard"
msgstr ""
