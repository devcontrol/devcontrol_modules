Módulo para actualizar los valores en base de datos de las compras ya hechas, para el correcto funcionamiento de compra en depósito.

Este módulo se debe ejecutar tras actualizar el módulo gestion_editorial con la release que incluye la compra en depósito. Después se puede desinstalar.

