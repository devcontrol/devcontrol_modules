# -*- coding: utf-8 -*-
######################################################################################
#
#         License**
#
########################################################################################
{
    'name': 'Actualiza compras hechas para compatibilidad con compra en deposito',
    'version': '13.0.0.0.1',
    'summary': 'Módulo para ajustar valores en la base datos para el correcto funcionamiento de compra en depósito.',
    'description': 'Módulo para ajustar valores en la base datos para el correcto funcionamiento de compra en depósito.',
    'category': 'Industries',
    'author': 'Colectivo DEVCONTROL',
    'author_email': 'devcontrol@sindominio.net',
    'maintainer': 'Colectivo DEVCONTROL',
    'company': 'Colectivo DEVCONTROL',
    'website': 'https://framagit.org/devcontrol',
    'depends': ['gestion_editorial'],
    'data': [
        'views/configura_datos_compra_en_deposito_view.xml'
    ],
    'images': ['static/description/icon.png'],
    'license': 'OPL-1',
    'price': 0,
    'currency': 'EUR',
    'installable': True,
    'application': False,
    'auto_install': False,
}
