# -*- coding: utf-8 -*-
######################################################################################
#
#         License**
#
########################################################################################
{
    'name': 'Pago derechos autoría',
    'version': '13.0.0.0.2',
    'summary': 'Módulo para los pagos de derechos de autoría para editoriales.',
    'description': 'Módulo para los pagos de derechos de autoría para editoriales.',
    'category': 'Industries',
    'author': 'Colectivo DEVCONTROL',
    'author_email': 'devcontrol@zici.fr',
    'maintainer': 'Colectivo DEVCONTROL',
    'company': 'Colectivo DEVCONTROL',
    'website': 'https://framagit.org/devcontrol',
    'depends': ['sale'],
    'data': [
        'views/derechos_autoria_boton_view.xml'
    ],
    'images': ['static/description/logo-devcontrol.png'],
    'license': 'OPL-1',
    'price': 0,
    'currency': 'EUR',
    'installable': True,
    'application': False,
    'auto_install': False,
}
