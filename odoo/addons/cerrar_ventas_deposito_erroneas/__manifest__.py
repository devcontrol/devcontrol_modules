# -*- coding: utf-8 -*-
######################################################################################
#
#         License**
#
########################################################################################
{
    'name': 'Cerrar ventas a depósito erróneas',
    'version': '13.0.0.0.1',
    'summary': 'Módulo Cerrar ventas a depósito erróneas para editoriales.',
    'description': 'Módulo Cerrar ventas a depósito erróneas para editoriales.',
    'category': 'Industries',
    'author': 'Colectivo DEVCONTROL',
    'author_email': 'devcontrol@zici.fr',
    'maintainer': 'Colectivo DEVCONTROL',
    'company': 'Colectivo DEVCONTROL',
    'website': 'https://framagit.org/devcontrol',
    'depends': ['sale'],
    'data': [
        'views/ventas_dep_erroneas_boton_view.xml'
    ],
    'images': ['static/description/logo-devcontrol.png'],
    'license': 'OPL-1',
    'price': 0,
    'currency': 'EUR',
    'installable': True,
    'application': False,
    'auto_install': False,
}
