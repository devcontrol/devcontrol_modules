# -*- coding: utf-8 -*-

# Do not edit the order of the imports;
# it's relevant for dependencies between them
from . import res_partner_descontrol
from . import res_company_descontrol
from . import res_config_settings_descontrol
from . import product_descontrol
from . import liquidacion_descontrol
from . import sale_order_descontrol
from . import stock_picking_descontrol
from . import purchase_order_descontrol
from . import product_pricelist_descontrol
from . import pos_order_descontrol
from . import stock_move_descontrol
