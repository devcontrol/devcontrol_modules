Módulo para generar DDAA para antiguos pedidos realizados desde POS.

Este módulo se debe ejecutar tras actualizar el módulo gestion_editorial con la release que incluye el fix para que los pedidos realizados desde POS generen DDAA. Se debe ejecutar exclusivamente si hay pedidos realizados desde POS antiguos a esta release.

