# Exportar Stock Editorial XLS

Extiende y adapta para gestión editorial el módulo "Export Product Stock in Excel v13" de Cybrosys Techno Solutions.
Este módulo te ayuda a obtener el informe actual de existencias de todos los productos en formato xls.

## Licencia

Este proyecto es software libre y está bajo la [Licencia GNU Affero General Public License versión 3 (AGPLv3)](https://www.gnu.org/licenses/agpl-3.0.en.html).

Esto significa que eres libre de utilizar este proyecto para cualquier propósito;
libre de estudiar y modificar este proyecto para satisfacer tus necesidades;
y libre de compartir este proyecto o tus modificaciones con cualquier persona.
Si compartes este programa o tus modificaciones debes otorgar a los receptores las mismas libertades.
Para ser más específico: debes compartir el código fuente bajo la misma licencia.


## Configuración 

No es necesaria ninguna configuración adicional. Simplemente realizar el proceso de instalación habitual para un módulo de Odoo.

## Problemas

Si encuentras algún problema o tienes una solicitud de nuevas funciones, por favor verifica primero en las [issues existentes](https://framagit.org/devcontrol/devcontrol_modules/-/issues) para ver si alguien ya ha reportado tu problema o solicitud. Si encuentras una issue relevante, puedes unirte a la discusión allí.

Si no encuentras una issue que aborde tu problema o solicitud, siéntete libre de [abrir una nueva issue](https://framagit.org/devcontrol/devcontrol_modules/-/issues/new). Asegúrate de proporcionar detalles completos y claros sobre el problema o la solicitud de características para que podamos entenderlo y abordarlo de manera efectiva.

## Créditos

Developers: Cybrosys Techno Solutions odoo@cybrosys.com

https://cybrosys.com

## Contacto

devcontrol@sindominio.net

