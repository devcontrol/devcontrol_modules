# -*- coding: utf-8 -*-

from . import get_file
from . import sinli_product_template
from . import res_partner_sinli
from . import res_config_settings_sinli
from . import sinli_message
from . import res_company_sinli
from . import sinli_purchase_order
