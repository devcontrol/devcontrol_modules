# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [13.0.0.9.0] - 2025-02-05

In this release the following modules have been updated: `gestion_editorial`, `exportar_stock_editorial_xls`

### Added
- New config setting: Default visibility limited by provider https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/143
- Process negative line in liquidations to return the products and adjust stock quantities https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/146
- Add ISBN in purchase orders reports https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/148
- Add button in contact view to print current stock in excel https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/150
- Add new fields (Autores, Libros entregados a autoría, Libros de promoción, Destruidos )in xls report "Product stock info" https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/151
- New DDAA page in product view + Enable more than one authorship receptor https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/153
- Update DDAA orders after product price update https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/154
- Divide authors location in 2 sub locations + new operation type (Entrega de libros de cortesía) https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/155
- Special purchase order (Albaran de autoría) for ddaa management https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/156
- Add config section for set default DDAA price + improve settings page view https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/160
- Add button for create DDAA order (Albarán de autoría) https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/162
- New view and menu for product tags and client type https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/164
- Add confirm message before saving product https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/165
- New ddaa order identificator (instead of partner ref) https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/166
- Deliver to authorship pricelist with no ddaa generation https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/167
- New config section to determine if new pricelists generate DDAA by default or not https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/169
- Confirmation date and order date will be shown in ddaa purchase orders 
- Added new column "Confirmation date" in purchase orders view.

### Changed
- Improve product type + tags field for products https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/145
- Modify info (i) in sales budgets. It also shows now the 'In distribution' quantity. https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/147
- Change order of main navigation menu https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/152
- Changed operation "Entrega libros autoría" https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/159
- DDAA product default price without taxes https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/168
- Pricelists menu is now at Sales->Configuration->Pricelists.

### Fixed
- Add odoo13_addon_account_invoice_pricelist==13.0.1.0.5 as requirement https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/142
- Fix liquidation naming issues https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/146
- Delete message when opening transfer https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/149

### To do after update (for users)
- Check manually if all ddaa receptors are okey. Specially if you have "dummy contacts" to group more than one author in one. 
- Manually check the price at which each author can buy the book. A default price of 0 has been set.
- Visit config section to set: Percentage to calculate DDAA product price. New pricelists generate DDAA by default or not. New products will have visibility limited by provider or not.

## [13.0.0.7.0] - 2024-07-26

In this release the following modules have been updated: `gestion_editorial`.

:warning: After update, please, remember that if you already had the data created manually, you need to remove the newly created records:
- Generic location for your company "GE"
- Location "Depósitos"
- Location "Autores"
- Location "Promoción"
- Picking type "Entrega de libros a autoría"
- Picking type "Rectificación de Liquidación"
- Picking type "Libros a promoción"

Also, in any case (with new db or old) in company's settings page review the new config in "venta depósito" and update it according to your needs.


### Added
- Inital and default data (locations, routes, picking types...) for new instalations and replacing of harcoded values https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/137
- Notify when pricelist is different than default client's pricelist in sale orders https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/136
- Add required modules to odoo dependecies https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/122


### Changed
- Disable buttons "Reabastecer" and "Existencias totales" in product view: https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/131
- Change sale order line product info in (i) icon https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/134
- Hide "Create Invoice" button for deposit sales https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/128


### Fixed
- Fix factura rectificativa type is_liquidation equal to false when creating from liquidation sales view button https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/138
- Fix POS orders now generate DDAA https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/129
- Fix / improve behavior of function "devolver_deposito_compras" https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/119/
- Fix error when returning deposit sales: https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/118


## [13.0.0.6.0] - 2024-03-26

In this release the following modules have been updated: `gestion_editorial`, `exportar_stock_editorial_xls`, `exportar_deposito_xls` and `configurar_compras_en_deposito`.

:warning: Once intalled this new release please follow these steps:
- Recargar traducciones. (Ajustes->Traduccciones->Idiomas) Click en actualizar y marcar "Sobrescribir términos existentes".
- Instalar el módulo 'Configura datos compra en deposito' para ajustar valores de compras hechas en la base datos para el correcto funcionamiento de compra en depósito. Ejecutar y comprobar valores. Desinstalar finalmente.
- Check that the stock picking type and the account journal created by the module are not duplicated due to a manual creation. Remove duplicated data.
- Configurar los parámetros de compra a deposito del panel de configuración.
- Cambiar nobre del picking type "recepciones" por "compra en firme".
- Cambiar picking type "Rectificación de Liquidación" por "Rectificación de Liquidación de venta".

### Added
- New functionality: purchase in deposit.
  - Use the purchase order lines to control the deposit purchases --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/62
  - New metrics for product quantities (visible in the product view and the stock report) --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/80
    - Ventas liquidadas: (anteriormente llamadas "Liquidados"): Los productos que hay en la localización Partner Location/Customers.
    - Compras liquidadas: La suma total de la cantidad liquidada de este producto que aparece en las líneas de pedidos de compra confirmados.
    - Recibidos: La suma total de la cantidad recibida de este producto que aparece en las líneas de pedidos de compra confirmados.
    - Depósito de compra: "Recibidos" menos "Compras liquidadas".
    - En almacén (anteriormente llamadas "A mano"): Los productos que hay en la localización Stock.
    - En distribución: Los productos que hay en la localización Depósito de venta.
    - Existencias totales (anteriormente llamadas "En propiedad"): Cantidad en almacén más cantidad en distribución.
  - Create the deposit reports for purchases, using the same wizard as used for the sale deposit reports -> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/66
  - New field for contacts 'Purchase type' used by default in purchase orders --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/105
  - Create configuration section for deposit purchase functionality, and create one picking type and one journal --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/75
  - New views for purchase deposit liquidación, and for returning purchase deposit (similar to the views in deposit sales) -> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/65
  - Add pricelist for purchase liquidation in contact (purchase section) --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/79
  - A pricelist must be selected for the purchase liquidaciones --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/95
  - The taxes of the liquidation lines are calculated correctly based on the price list (PVP) --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/86
  - Set the default journal in purchase liquidations --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/78 and https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/99
  - Create wizard for purchase liquidaciones, that throws error when selected quantities are not allowed --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/109
  - In wizard for returning liquidaciones (sale and purchase) adapt column titles --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/108
  - Check available stock in returning diposit purchases --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/71
  - Prevent products with type service from being purchased by with the picking type "Compra en depósito" --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/104
- New product field that allows toggle limit visibility by provider or not --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/107
- New module created `configura_datos_compra_en_deposito` for adjusting the values of the liquidated amounts of the purchases existing at the time of the update/installation of the module --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/76

### Changed
- Show "Albaran de devolución" only in deposit return invoices --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/74
- Move field "picking type" in purchase order view --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/60
- Filter products by provider in purchase orders --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/87
- Move contact type field to the general contact view --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/98
- Change product metrics titles: --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/97
  - "En propiedad" -> "Existencias totales"
  - "En Stock" ("A mano") -> "En almacén"
- Change column titles in deposit reports --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/96
- Hide some fields in liquidation views --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/103

### Fixed
- Add error message when selecting no contact for individual deposit report --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/91
- Remove unused constant --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/101
- Fix bug purchase order lines --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/102
- Handle when return picking is empty --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/118
- Group invoice lines by product and sum quantities --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/119


## [13.0.0.5.1] - 2024-03-14

In this release the following modules have been updated: `gestion_editorial`, `exportar_stock_editorial_xls`, `exportar_deposito_xls`, `pago_derechos_autoria` and `sinli`.

And a new module created: `cerrar_ventas_deposito_erroneas`.

### Added
- New module created for fixing the incorrect direct sales that were wrongly created as deposit sales --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/70
- Added Devcontrol logo to modules `cerrar_ventas_deposito_erroneas`, `pago_derechos_autoria` and `sinli` --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/92

### Changed
- Use PVP price in stock reports instead of the cost price --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/83
- Change calculation of field "owned" of products. Now it is `on_hand_qty + in_distribution_qty` --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/84
- Show stock quantity in mini-view of products instead of "on hand" quantity --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/85
- Update `gestion_editorial` module description --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/93
- Hide "Update quantities" button in prodct view --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/100
- Set no supplier taxes in DDAA products by default --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/110
- Set incoming picking type in ddaa purchases --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/111

### Fixed
- Fix error when dividing by 0 (when product PVP is 0) --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/72
- Remove wrong help text in liquidated quantities field in the product view --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/82
- Fix button name in product view --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/88
- ERROR al importar productos --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/113


## [13.0.0.5.0] - 2023-11-29

### Added
- Añade configuración para DDAA y amplia funcionalidad con categoria llibres mare. IMPORTANTE! :warning: Lee pasos para actualizar el módulo `gestion_editorial` --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/51

### Changed
- Contar con productos en ubicaciones hijas para números de "En stock" y "En distribución" --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/61
- Revisar campos en facturas impresas --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/64


## [13.0.0.4.1] - 2023-10-18

### Added
- Genera DDAA si no se añaden cantidades hechas manualmente --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/58

### Changed
- Resolve "Corregir números de "A mano", "Previsto", "Comprado", "Vendido" en ficha producto y informe d'stock" --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/54
 
### Fixed
- Elimina lineas de permisos para modelo Lineas Liquidacion --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/57


## [13.0.0.4.0] - 2023-08-09

### Added
- Informe Resumen de depósitos de clientes, compatible con exportar stock xls --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/39
- Añade menu y vista para Devolución Liquidacion --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/47
- Añade la posibilidade de Devoluiciones de depósito --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/47
- Añade módulo nuevo de SINLI --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/41
- Las tarifas permiten asociar rutas  --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/50

### Changed
- Permite ddaa en negativo --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/45
- Mejorar cómo se calcula el unit_price en líneas de venta y de liquidación --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/53
- Mejorar documentación de realease

### Fixed

- Se borra el código de logging, que estaba causando errores. --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/38 
- Borra filtro de receptora_derecho_autoria solo a autores --> https://framagit.org/devcontrol/devcontrol_modules/-/merge_requests/40

